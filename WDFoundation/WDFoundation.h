//
//  WDFoundation.h
//  WDFoundation
//
//  Created by Jorge Galrito on 13/04/2018.
//  Copyright © 2018 WATERDOG. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WDFoundation.
FOUNDATION_EXPORT double WDFoundationVersionNumber;

//! Project version string for WDFoundation.
FOUNDATION_EXPORT const unsigned char WDFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WDFoundation/PublicHeader.h>


