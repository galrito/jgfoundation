//
//  AuthorizationController.swift
//  WDFoundation
//
//  Created by Jorge Galrito on 13/04/2018.
//  Copyright © 2018 WATERDOG. All rights reserved.
//

import Foundation
import os.log

@available(iOS 10.0, *)
let authenticationLog = OSLog(subsystem: "mobi.waterdog.WDFoundation", category: "Authentication")

public protocol UserAccess: Codable {
  associatedtype User: Codable
  var user: User { get }
  var authorizationToken: String { get }
}

public protocol AuthorizationControllerProtocol {
  associatedtype Access: UserAccess
  
  var isAuthenticated: Bool { get }
  var userAccess: Access? { get }
  var authorizationToken: String? { get }
  
  func store(userAccess: Access)
  func removeUserAccess()
}

extension AuthorizationControllerProtocol {
  public var isAuthenticated: Bool {
    return userAccess != nil
  }
  
  public var authorizationToken: String? {
    return userAccess?.authorizationToken
  }
}

open class AuthorizationController<Access: UserAccess>: AuthorizationControllerProtocol {
  public final var userAccess: Access? {
    if #available(iOS 10, *) {
      os_log("Preparing to retrieve user credentials stored on disk", log: authenticationLog, type: .debug)
    }
    
    if let cachedUserAccess = _cachedUserAccess {
      if #available(iOS 10, *) {
        os_log("Returning cached UserAccess: \"%@\"", log: authenticationLog, type: .debug, cachedUserAccess.authorizationToken)
      }
      return cachedUserAccess
    }
    
    do {
      let userAccessData = try Data(contentsOf: storeURL)
      let propertyListDecoder = PropertyListDecoder()
      let userAccess = try propertyListDecoder.decode(Access.self, from: userAccessData)
      _cachedUserAccess = userAccess
      return userAccess
    } catch {
      if #available(iOS 10, *) {
        os_log("Error retrieving stored user access: \"%@\"", log: authenticationLog, error.localizedDescription)
      }
      _cachedUserAccess = nil
      return nil
    }
  }
  
  private var _cachedUserAccess: Access?
  
  public init() {
  }
  
  public final func store(userAccess: Access) {
    if #available(iOS 10, *) {
      os_log("Preparing to store user credentials: %@", log: authenticationLog, type: .debug, userAccess.authorizationToken)
    }
    do {
      let encoder = PropertyListEncoder()
      let userAccessData = try encoder.encode(userAccess)
      try userAccessData.write(to: storeURL)
      if #available(iOS 10, *) {
        os_log("User credentials stored correctly on disk", log: authenticationLog, type: .default)
      }
      NotificationCenter.default.post(name: .userDidAuthenticateNotification, object: self)
    } catch {
      if #available(iOS 10, *) {
        os_log("Error saving credentials to disk: \"%{public}@\"", log: authenticationLog, type: .error, error.localizedDescription)
      }
    }
  }
  
  public final func removeUserAccess() {
    try? FileManager.default.removeItem(at: storeURL)
    NotificationCenter.default.post(name: .userDidLogoutNotification, object: self)
  }
  
  private var storeURL: URL {
    let applicationSupportURL = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    return applicationSupportURL.appendingPathComponent("UserAccess.plist")
  }
  
}

extension Notification.Name {
  public static let userDidAuthenticateNotification = Notification.Name(rawValue: "UserDidAuthenticateNotification")
  public static let userDidLogoutNotification = Notification.Name(rawValue: "UserDidLogoutNotification")
}
