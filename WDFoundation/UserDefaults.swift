//
//  SegueHandling.swift
//  WDFoundation
//
//  Created by Jorge Galrito on 13/04/2018.
//  Copyright © 2018 WATERDOG. All rights reserved.
//

import Foundation

extension UserDefaults {
  public struct KeyName: RawRepresentable {
    public var rawValue: String
    public init?(rawValue: String) {
      self.rawValue = rawValue
    }
  }
  
  public func object<T>(of type: T.Type, for name: KeyName) -> T? {
    return object(forKey: name.rawValue) as? T
  }
  
  public func set<T>(value: T, for name: KeyName) {
    set(value, forKey: name.rawValue)
  }
}
