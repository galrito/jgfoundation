//
//  AuthenticationOperation.swift
//  WDFoundation
//
//  Created by Jorge Galrito on 15/04/2018.
//  Copyright © 2018 WATERDOG. All rights reserved.
//

import Foundation
import WDOperations
import os.log

internal class AnyAuthenticationOperation: WDOperation {
}

/**
 *  `AuthenticationOperation` tries to authenticate with the server,
 *  represented by the `RemoteServiceAuthentication` protocol.
 *
 *  Upon receving the response from the remote server, the operation
 *  calls the `completion` block with an authentication token
 *  or nil.
 */
final class AuthenticationOperation<Access: UserAccess>: AnyAuthenticationOperation {
  
  let remote: AuthenticationRemoteService
  let userCredentials: UserCredentials
  let completionHandler: (Access?, AuthenticationError?) -> Void
  let session: URLSession
  private var task: URLSessionTask?
  
  init(remote: AuthenticationRemoteService, credentials: UserCredentials, session: URLSession, completion: @escaping (Access?, AuthenticationError?) -> Void) {
    self.remote = remote
    self.userCredentials = credentials
    self.session = session
    self.completionHandler = completion
    super.init()
    if #available(iOS 10, *) {
      os_log("Initializing AuthenticationOperation", log: authenticationLog, type: .debug)
    }
  }
  
  override func execute() {
    if #available(iOS 10.0, *) {
      os_log("Start execution of AuthenticationOperation", log: authenticationLog)
      os_log("Sending credentials to remote service:\n   %@ / %@", log: authenticationLog, type: .debug, userCredentials.username, userCredentials.password)
    }
    let request = remote.login(with: userCredentials)
    let task = session.dataTask(with: request) { data, response, _ in
      defer {
        if #available(iOS 10, *) {
          os_log("AuthenticationOperation will finish", log: authenticationLog, type: .debug)
        }
        self.finish()
      }
      
      if #available(iOS 10, *) {
        os_log("Callback execution", log: authenticationLog, type: .info)
      }
      
      if #available(iOS 10, *) {
        os_log("Trying to decode UserAccess instance", log: authenticationLog, type: .debug)
      }
      
      guard let httpResponse = response as? HTTPURLResponse, let data = data else {
        self.completionHandler(nil, .hardError("An error occurred connecting to the service"))
        return
      }
      
      var extractedUserAccess: Access?
      do {
        let decoder = JSONDecoder()
        extractedUserAccess = try decoder.decode(Access.self, from: data)
      } catch {
        if #available(iOS 10.0, *) {
          os_log("AuthenticationOperation received an error trying to decode UserAccess: \"%{public}@\"", log: authenticationLog, type: .error, error.localizedDescription)
        }
        
        let error = self.remote.error(fromStatusCode: httpResponse.statusCode, data: data)
        self.completionHandler(nil, error ?? .hardError("An error occurred"))
        return
      }
      
      guard let access = extractedUserAccess else {
        return
      }
      
      if #available(iOS 10, *) {
        os_log("AuthenticationOperation received an authorization token: \"%@\"", log: authenticationLog, type: .info, access.authorizationToken)
      }
      self.completionHandler(access, nil)
    }
    self.task = task
    task.resume()
  }
  
  override func cancel() {
    task?.cancel()
    super.cancel()
  }
}
